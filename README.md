# o2b

_o2b_ is a utility that reads outlook contact CSV files and operates with them on Bexio:

- `o2b check`: Reads a CSV on STDIN and prints a CSV to STDOUT for all contacts of the CSV, which are not in Bexio yet.
  It uses the email address as unique identifier.
- `o2b import`: Reads a CSV on STDIN and imports all the contacts as persons into Bexio.
  It only imports the _Last Name_, _First Name_ and the _Email Address_.

## Usage

For this tool to be helpful, use it like this:

1. Get some contacts as CSV in the outlook format.
   You can export this format from Google Contacts for example.
2. Ask the tool to check which contacts are in fact missing:
   ```bash
   cat contacts.csv | \
   o2b -C $CompanyID -U $UserID -P $PublicKey -S $SignatureKey check \
   > missing_contacts.csv
   ```
   Replace the values starting with a `$` with the actual values.
3. Check the `missing_contacts.csv` file and delete anyone you don't want to import into Bexio.
4. Import the missing contacts:
   ```bash
   cat missing_contacts.csv | \
   o2b -C $CompanyID -U $UserID -P $PublicKey -S $SignatureKey import
   ```

A full example may look like this:

```bash
$ cat contacts.csv
First Name,Middle Name,Last Name,Title,Suffix,Initials,Web Page,Gender,Birthday,Anniversary,Location,Language,Internet Free Busy,Notes,E-mail Address,E-mail 2 Address,E-mail 3 Address,Primary Phone,Home Phone,Home Phone 2,Mobile Phone,Pager,Home Fax,Home Address,Home Street,Home Street 2,Home Street 3,Home Address PO Box,Home City,Home State,Home Postal Code,Home Country,Spouse,Children,Manager's Name,Assistant's Name,Referred By,Company Main Phone,Business Phone,Business Phone 2,Business Fax,Assistant's Phone,Company,Job Title,Department,Office Location,Organizational ID Number,Profession,Account,Business Address,Business Street,Business Street 2,Business Street 3,Business Address PO Box,Business City,Business State,Business Postal Code,Business Country,Other Phone,Other Fax,Other Address,Other Street,Other Street 2,Other Street 3,Other Address PO Box,Other City,Other State,Other Postal Code,Other Country,Callback,Car Phone,ISDN,Radio Phone,TTY/TDD Phone,Telex,User 1,User 2,User 3,User 4,Keywords,Mileage,Hobby,Billing Information,Directory Server,Sensitivity,Priority,Private,Categories
Thomas,,Locomotive,,,,,,,,,,,,thomas.locomotive@nxt.engineering,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,
Bert,,Becker,,,,,,,,,,,,bert.becker@nxt.engineering,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,

$ cat contacts.csv | \
    o2b -C abc012345 -U 1 -P abcABC09871234BBC -S bcaPOI567iopAAC check |
    tee missing_contacts.csv
Last Name,First Name,E-mail Address
Becker,Bert,bert.becker@nxt.engineering

$ cat missing_contacts.csv | \
    o2b -C abc012345 -U 1 -P abcABC09871234BBC -S bcaPOI567iopAAC import

$ rm contacts.csv missing_contacts.csv
```

## Commands

Run `o2b` or `o2b help` to see also recently introduced commands:

```
NAME:
   o2b - Import outlook contacts csv to bexio

USAGE:
   o2b [global options] command [command options] [arguments...]

VERSION:
   0.0.0

AUTHOR:
   Christian Mäder <christian.maeder@nxt.engineering>

COMMANDS:
   check, c   Reads a CSV on STDIN and creates a CSV on STDOUT of all the contacts which were not found in Bexio.
   import, i  Reads a CSV on STDIN and creates a new contact for every entry on STDOUT.
   debug, d   Displays the configuration as understood by the program.
   help, h    Shows a list of commands or help for one command

GLOBAL OPTIONS:
   --CompanyID value, -C value     The Company ID as assigned by Bexio
   --UserID value, -U value        The User ID as assigned by Bexio (default: 0)
   --PublicKey value, -P value     The Public Key as assigned by Bexio
   --SignatureKey value, -S value  The Signature Key as assigned by Bexio
   --help, -h                      show help
   --version, -v                   print the version

COPYRIGHT:
   (c) 2019 nxt Engineering GmbH
```

## Installation

The following command should install _o2b_ on your computer.
You need to have a recent version of Go installed on your computer for this to work.
Also make sure that the `$GOPATH` part of your `$PATH`.

```bash
go get gitlab.com/nxt/public/o2b
``` 

You will also need an API key for your Bexio instance.
You can get it on the following page:
https://office.bexio.com/index.php/client_settings/apiKey_list
