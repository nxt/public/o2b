module gitlab.com/nxt/public/o2b

go 1.19

require (
	github.com/gocarina/gocsv v0.0.0-20220927221512-ad3251f9fa25
	github.com/urfave/cli v1.22.10
	github.com/urfave/cli/v2 v2.19.2
	gitlab.com/nxt/public/bexio v0.1.1
)

require (
	github.com/cpuguy83/go-md2man/v2 v2.0.2 // indirect
	github.com/go-resty/resty/v2 v2.7.0 // indirect
	github.com/russross/blackfriday/v2 v2.1.0 // indirect
	github.com/shurcooL/sanitized_anchor_name v1.0.0 // indirect
	golang.org/x/net v0.0.0-20211029224645-99673261e6eb // indirect
)
