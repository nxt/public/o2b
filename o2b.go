package main

import (
	"bufio"
	"encoding/csv"
	"io"
	"log"
	"os"

	"gitlab.com/nxt/public/bexio"

	"github.com/gocarina/gocsv"
	"github.com/urfave/cli"
)

func main() {
	log.SetOutput(os.Stderr)
	ioReader := bufio.NewReader(os.Stdin)

	ConfigureCSVReader()

	app := cli.NewApp()
	app.Version = "0.1.0"
	app.Name = "o2b"
	app.Usage = "Import outlook contacts csv to bexio"
	app.Author = "Christian Mäder <christian.maeder@nxt.engineering>"
	app.Copyright = "(c) 2019 nxt Engineering GmbH"
	app.EnableBashCompletion = true

	flags := []cli.Flag{
		cli.StringFlag{
			Name:  "CompanyID, C",
			Usage: "The Company ID as assigned by Bexio",
		},
		cli.IntFlag{
			Name:  "UserID, U",
			Usage: "The User ID as assigned by Bexio",
		},
		cli.StringFlag{
			Name:  "PublicKey, P",
			Usage: "The Public Key as assigned by Bexio",
		},
		cli.StringFlag{
			Name:  "SignatureKey, S",
			Usage: "The Signature Key as assigned by Bexio",
		},
	}
	app.Flags = flags

	app.Commands = []cli.Command{
		{
			Name:    "check",
			Aliases: []string{"c"},
			Usage: "Reads a CSV on STDIN and creates a CSV on STDOUT of all " +
				"the contacts which were not found in Bexio.",
			Action: func(c *cli.Context) error {
				return FindMissingContacts(ioReader, createBexioClient(c))
			},
		},
		{
			Name:    "import",
			Aliases: []string{"i"},
			Usage:   "Reads a CSV on STDIN and creates a new contact for every entry on STDOUT.",
			Action: func(c *cli.Context) error {
				return ImportContacts(ioReader, createBexioClient(c), c.GlobalInt("UserID"))
			},
		},
		{
			Name:    "debug",
			Aliases: []string{"d"},
			Usage:   "Displays the configuration as understood by the program.",
			Action: func(c *cli.Context) error {
				log.Printf("%#v", buildAPIToken(c))
				return nil
			},
		},
	}

	err := app.Run(os.Args)
	if err != nil {
		log.Fatal(err)
	}
}

func createBexioClient(c *cli.Context) *bexio.Client {
	client := bexio.NewClient(buildAPIToken(c))

	return &client
}

func buildAPIToken(c *cli.Context) bexio.APIToken {
	return bexio.APIToken{
		CompanyID:    c.GlobalString("CompanyID"),
		UserID:       c.GlobalInt("UserID"),
		PublicKey:    c.GlobalString("PublicKey"),
		SignatureKey: c.GlobalString("SignatureKey"),
	}
}

func ConfigureCSVReader() {
	gocsv.SetCSVReader(func(ioReader io.Reader) gocsv.CSVReader {
		csvReader := csv.NewReader(ioReader)
		csvReader.FieldsPerRecord = -1
		return csvReader
	})
}

func ImportContacts(ioReader *bufio.Reader, client *bexio.Client, userID int) error {
	contacts, err := readCSVFromStdin(ioReader)
	if err != nil {
		return err
	}

	for _, contact := range contacts {
		contact.UserID = userID
		contact.OwnerID = userID
		contact.ContactType = bexio.ContactTypePerson

		err = client.CreateContact(contact)

		if err != nil {
			log.Printf("Can't create contact '%s': %e", contact.Email, err)
		}
	}
	return nil
}

func FindMissingContacts(ioReader *bufio.Reader, client *bexio.Client) error {
	contacts, err := readCSVFromStdin(ioReader)
	if err != nil {
		return err
	}

	var missingContacts []bexio.Contact
	for _, contact := range contacts {
		if contact.FirstName == "" || contact.LastName == "" || contact.Email == "" {
			continue
		}

		found := CheckEachContact(contact, client)

		if !found {
			missingContacts = append(missingContacts, contact)
		}
	}

	err = gocsv.Marshal(missingContacts, os.Stdout)
	return err
}

func readCSVFromStdin(ioReader *bufio.Reader) ([]bexio.Contact, error) {
	var contacts []bexio.Contact
	err := gocsv.Unmarshal(ioReader, &contacts)
	if err != nil {
		return nil, err
	}
	return contacts, nil
}

func CheckEachContact(contact bexio.Contact, client *bexio.Client) bool {
	found, err := SearchContactByEmailInBexio(client, contact)
	if err != nil {
		log.Printf("Error checking '%s': %e", contact.Email, err)
		return false
	}

	return found
}

func SearchContactByEmailInBexio(client *bexio.Client, contact bexio.Contact) (bool, error) {
	contacts, err := client.SearchContacts(map[string]interface{}{
		"mail": contact.Email,
	})
	if err != nil {
		return false, err
	}
	if len(contacts) > 0 {
		return true, nil
	}

	contacts, err = client.SearchContacts(map[string]interface{}{
		"mail_second": contact.Email,
	})
	if err != nil {
		return false, err
	}
	return len(contacts) > 0, nil
}
